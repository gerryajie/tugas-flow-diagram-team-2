const orderkfc = [
  "kfc",
  "hokben",
  "pizza",
  "tunai",
  "ewallet",
  "tersedia",
  "gopay",
];

function bukaHandphone() {
  console.log("Buka Handphone");
}

function bukaAplikasi() {
  console.log("Buka Aplikasi");
}
function pilihMenu() {
  if (orderkfc.includes("kfc")) {
    console.log("memilih Kfc");
    return true;
  }
  console.log("Resto tidak ada");
  return false;
}

function restoTidakada() {
  console.log("Balik ke menu");
  console.log("Pilih resto yang lain");
  return true;
}

function tambahKeranjang() {
  console.log("Keranjang sudah berisi order");
}

function metodeBayar() {
  if (orderkfc.includes("gopay")) {
    console.log("Pilih metode pembayaran via gopay");
    return true;
  }
  console.log("Pilih metode cash");
  return false;
}

function siapCash() {
  console.log("Siapkan uang cash");
}

function klikOrder() {
  console.log("Klik order");
}

function cariDriver() {
  if (orderkfc.includes("tersedia")) {
    console.log("Driver tersedia");
    console.log("Makanan sedang di antar.");
    return true;
  }
  console.log("Driver tidak ada.");
  console.log("Pergi cari makan di luar.");
}

function makanDiluar() {
  console.log("Akhirnya makan diluar.");
}

function makan() {
  console.log("Selamat menikmati makanan.!!");
}

function start() {
  bukaHandphone();
  bukaAplikasi();
  !pilihMenu() && restoTidakada();
  tambahKeranjang();
  !metodeBayar() && siapCash();
  klikOrder();
  !cariDriver() && makanDiluar();
  makan();
}

start();
